<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

use App\ChatMessage;
use App\Events\ChatMessageWasReceived;
use App\User;
use Illuminate\Http\Request;

Route::get('/', function () {
    return view('welcome');
});

Route::post('message', function (Request $request)
{
    $user = Auth::user();
    if(!$user){
        $generator = Faker\Factory::create();
        $user      = User::create([
            'name' => $generator->name,
            'email' => $generator->email,
            'password' => $generator->password
        ]);
    }

    $message = ChatMessage::create([
        'user_id' => $user->id,
        'message' => $request->input('message')
    ]);

    event(new ChatMessageWasReceived($message, $user));
});
