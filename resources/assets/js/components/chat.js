/**
 * Created by pp on 10-29-2016.
 */
module.exports = {
    data() {
        return {
            posts: [],
            newMsg: ''
        }
    },
    ready() {
        Echo.channel('public-test-channel')
            .listen('ChatMessageWasReceived', (data) => {
                // push ata to posts list
                this.posts.push({
                    message: data.chatMessage.message,
                    username: data.user.name
                });
            });
    },

    methods: {
        press(){
            this.$http.post('/message/', {message: this.newMsg})
                .then((response) => {
                    this.newMsg = '';
                });
        }
    }

};
