<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
        <script type="text/javascript">
            window.Laravel = <?= json_encode(['csrfToken'=>csrf_token()]);?>
        </script>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    <a href="{{ url('/login') }}">Login</a>
                    <a href="{{ url('/register') }}">Register</a>
                </div>
            @endif

            <div class="content" id="app">
                <div class="title m-b-md">
                    <h1>Chat</h1>
                </div>
                    <chat inline-template>
                        <div class="panel-body">
                            You are logged in!
                            <hr>
                            <h2>Write something to all users</h2>
                            <input type="text" class="form-control" placeholder="something" required="required"
                                   v-model="newMsg" @keyup.enter="press">

                            <hr>
                            <h3>Messages</h3>
                            <ul v-for="post in posts">
                                <li><strong>@{{ post.username }} says:</strong> @{{ post.message }}</li>
                            </ul>
                        </div><!-- panel-body -->
                    </chat>
            </div>
        </div>
        <script type="text/javascript" src="{{ elixir('js/app.js') }}"></script>
    </body>
</html>
